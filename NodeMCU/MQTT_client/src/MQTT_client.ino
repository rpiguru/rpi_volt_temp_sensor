#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "MCP9808.h"

const char* device_name = "Node2";   	// Change this value for each WiFi devices
int upload_interval = 5000;       		// Uploading interval of data in miliseconds.
float divide_factor = 22.36;          // Voltage divider factor

/*    --------  Constant values -----------        */
const char* ssid = "RPi3-AP";              // RPi3's AP name
const char* password = "raspberry";        // RPi3's AP password
const char* mqtt_server = "172.24.1.1";    // Server has been built on the router(RPi 3) itself

const char* temp_suffix = "/temperature";
const char* volt_suffix = "/voltage";

// Create the MCP9808 temperature sensor object
MCP9808 tempsensor = MCP9808();

WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;

int LED_PIN = 16; 			// Built-in LED pin
char buf_pub_topic[50];
char buf_sub_topic[50];

void setup() {
  pinMode(A0, INPUT);     // Initialize the A0 pin for voltage divider
  pinMode(LED_PIN, OUTPUT);     // Initialize the LED pin to indicate data uploading
  digitalWrite(LED_PIN, LOW);

  if (!tempsensor.begin()) {
    Serial.println("Couldn't find MCP9808!");
  }
  tempsensor.shutdown_wake(0);   // Don't remove this line! required before reading temp
  Serial.begin(9600);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
//  client.setCallback(callback);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_PIN, LOW);
    delay(250);
    digitalWrite(LED_PIN, HIGH);
    delay(250);
    Serial.print(".");
  }

  digitalWrite(LED_PIN, LOW);
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > upload_interval) {
    digitalWrite(LED_PIN, LOW);
    int s_value = analogRead(A0);
  	float voltage = s_value / 1024.0 * divide_factor;
  	Serial.print("Voltage: "); Serial.println(voltage);

  	float temp_c = tempsensor.readTempC();
  	float temp_f = temp_c * 9.0 / 5.0 + 32;
  	Serial.print("Temp: "); Serial.print(temp_c); Serial.print(" *C\t"); Serial.print(temp_f); Serial.println(" *F");

  	// Publish temperature value
  	char* buf_temp = new char[10];
  	dtostrf(temp_c, 5, 2, buf_temp);
  	set_pub_topic(temp_suffix);
  	client.publish(buf_pub_topic, buf_temp);

  	// Publish voltage value
  	char* buf_volt = new char[10];
  	dtostrf(voltage, 5, 2, buf_volt);
  	set_pub_topic(volt_suffix);
  	client.publish(buf_pub_topic, buf_volt);
    delay(100);
  	digitalWrite(LED_PIN, HIGH);
    lastMsg = now;
  }
  else{
    delay(100);
  }
}

void set_pub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      client.subscribe("common");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

//void callback(char* topic, byte* payload, unsigned int length) {
//  Serial.print("Message arrived [");
//  Serial.print(topic);
//  Serial.print("] ");
//  for (int i = 0; i < length; i++) {
//    Serial.print((char)payload[i]);
//  }
//  Serial.println("");
//
//  // Check topic of windows open relay
//  set_sub_topic(w_open_set_suffix);
//  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
//    if (!strncmp((const char*)payload, "ON", 2)) {
////      if (!strncmp(w_close_state, "ON", 2)) {
////        Serial.println("WINDOWS CLOSE is already on, try again later...");
////      }
////      else{
//        w_open_state = "ON";
//        Serial.println("Turning RELAY of WINDOWS OPEN ON...");
//        digitalWrite(PIN_W_OPEN, HIGH);
////        t_start_open = millis();
////      }
//    } else if (!strncmp((const char*)payload, "OFF", 3)){
//      w_open_state = "OFF";
//      Serial.println("Turning RELAY of WINDOWS OPEN OFF...");
//      digitalWrite(PIN_W_OPEN, LOW);
//    }
//  }
//  // Check topic of windows close relay
//  else{
//    set_sub_topic(w_close_set_suffix);
//    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
//      if (!strncmp((const char*)payload, "ON", 2)) {
////        if (!strncmp(w_open_state, "ON", 2)) {
////          Serial.println("WINDOWS OPEN is already on, try again later...");
////        }
////        else{
//            w_close_state = "ON";
//            Serial.println("Turning RELAY of WINDOWS CLOSE ON...");
//            digitalWrite(PIN_W_CLOSE, HIGH);
////            t_start_close = millis();
////        }
//      } else if (!strncmp((const char*)payload, "OFF", 3)){
//        w_close_state = "OFF";
//        Serial.println("Turning RELAY of WINDOWS CLOSE OFF...");
//        digitalWrite(PIN_W_CLOSE, LOW);
//      }
//    }
//  }
//}


void set_sub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}
