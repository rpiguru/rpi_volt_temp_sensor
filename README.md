# Voltage & Temperature sensor for 12V Batteries.

## Components

- Raspberry Pi3
    
    https://www.amazon.com/CanaKit-Raspberry-Ultimate-Starter-Kit/dp/B01C6Q4GLE/ref=sr_1_7?s=pc&ie=UTF8&qid=1478846416&sr=1-7&keywords=raspberry+pi+3
    
- NodeMCU V1
    
    https://www.amazon.com/NodeMcu-Internet-Things-Development-ESP8266/dp/B01I9FR528/ref=sr_1_6?ie=UTF8&qid=1476989760&sr=8-6&keywords=nodemcu
    
- Adafruit MCP9808 Temperature Sensor
    
    https://www.adafruit.com/product/1782
    
    https://www.amazon.com/Adafruit-HTU21D-F-Temperature-Humidity-Breakout/dp/B00OKJFLWO/ref=sr_1_2?ie=UTF8&qid=1479423527&sr=8-2&keywords=mcp9808
    
- Voltage meter
    
    We need to compose voltage divider circuit because NodeMCU's analog pin can read DC voltage range of 0 ~ 1V.
    
    I used 2 resistors (2.2KOhm, 47KOhm: most popular values), so that the dividing value is:
        
        (2.2 + 47) / 2.2 = 22.3636 
    
    So we can measure DC voltage up to 22.36V.
    
- Power supply
    
    After reading the datasheet of power regulator of NodeMCU, the power supply pin (VIN) should be in range of 4.75 ~ 10V.
    
    So it is ideal to use normal 12V -> 5V DC-DC convert module.
    
    https://www.amazon.com/DROK-Numerical-Switching-Adjustable-Stabilizers/dp/B00BYTEHQO

## Wiring all components.
  
### Wiring with MCP9808 temperature sensor

| **NodeMCU** | **MCP9808**|
| :----:      |   :----:   |
|  GND        |    GND     | 
|  3.3V       |    VDD     | 
|  GPIO4 (D2) |    SDA     |
|  GPIO5 (D1) |    SCL     |

![Wiring on BreadBoard](img/fritzing_bb.jpg "Wiring on BreadBoard")

![Wiring schematic](img/fritzing_schem.jpg "Wiring schematic")
    

## Configure RPi 3

### Setup WiFi AP on the Raspberry Pi 3

- Install packages.

        sudo apt-get install dnsmasq hostapd

- CONFIGURE YOUR INTERFACES

    The first thing you'll need to do is to configure your `wlan0` interface with a static IP.

    In newer Raspian versions, interface configuration is handled by `dhcpcd` by default. 
    We need to tell it to ignore `wlan0`, as we will be configuring it with a static IP address elsewhere. 

    So open up the `dhcpcd` configuration file with `sudo nano /etc/dhcpcd.conf` and add the following line to the bottom of the file:

        denyinterfaces wlan0  
    
    Note: This must be ABOVE any interface lines you may have added!

    Now we need to configure our static IP. 

    To do this open up the interface configuration file with `sudo nano /etc/network/interfaces` and edit the `wlan0` section so that it looks like this:

        allow-hotplug wlan0  
        iface wlan0 inet static  
            address 172.24.1.1
            netmask 255.255.255.0
            network 172.24.1.0
            broadcast 172.24.1.255
        #    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
    
    Restart `dhcpcd` with `sudo service dhcpcd restart` and then reload the configuration for `wlan0` with `sudo ifdown wlan0; sudo ifup wlan0`.

- CONFIGURE HOSTAPD
    
    Next, we need to configure `hostapd`. 
    
    Create a new configuration file with `sudo nano /etc/hostapd/hostapd.conf` with the following contents:

        # This is the name of the WiFi interface we configured above
        interface=wlan0
        
        # Use the nl80211 driver with the brcmfmac driver
        driver=nl80211
        
        # This is the name of the network
        ssid=RPi3-AP
        
        # Use the 2.4GHz band
        hw_mode=g
        
        # Use channel 6
        channel=6
        
        # Enable 802.11n
        ieee80211n=1
        
        # Enable WMM
        wmm_enabled=1
        
        # Enable 40MHz channels with 20ns guard interval
        ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]
        
        # Accept all MAC addresses
        macaddr_acl=0
        
        # Use WPA authentication
        auth_algs=1
        
        # Require clients to know the network name
        ignore_broadcast_ssid=0
        
        # Use WPA2
        wpa=2
        
        # Use a pre-shared key
        wpa_key_mgmt=WPA-PSK
        
        # The network passphrase
        wpa_passphrase=raspberry
        
        # Use AES, instead of TKIP
        rsn_pairwise=CCMP
    
    NOTE: We assigned **SSID & PASSWORD** as `RPi3-AP & raspberry` and these values will be embedded into the firmware of ESP.
        
    We can check if it's working at this stage by running `sudo /usr/sbin/hostapd /etc/hostapd/hostapd.conf`. 
    
    If it's all gone well thus far, you should be able to see to the network **RPi3-AP**! 
    
    If you try connecting to it, you will see some output from the Pi, but you won't receive and IP address until we set up `dnsmasq` in the next step. 
    
    Use **Ctrl+C** to stop it.

    We aren't quite done yet, because we also need to tell `hostapd` where to look for the config file when it starts up on boot. 
    
    Open up the default configuration file with `sudo nano /etc/default/hostapd` and find the line `#DAEMON_CONF=""` and replace it with `DAEMON_CONF="/etc/hostapd/hostapd.conf"`.
    
- CONFIGURE DNSMASQ
    
    The shipped `dnsmasq` config file contains a wealth of information on how to use it, but the majority of it is largely redundant for our purposes. 
    
    I'd advise moving it (rather than deleting it), and creating a new one with

        sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig  
        sudo nano /etc/dnsmasq.conf  
    
    Paste the following into the new file:

        interface=wlan0                 # Use interface wlan0  
        listen-address=172.24.1.1       # Explicitly specify the address to listen on  
        bind-interfaces                 # Bind to the interface to make sure we aren't sending things elsewhere  
        server=8.8.8.8                  # Forward DNS requests to Google DNS  
        domain-needed                   # Don't forward short names  
        bogus-priv                      # Never forward addresses in the non-routed address spaces.  
        dhcp-range=172.24.1.50,172.24.1.150,12h # Assign IP addresses between 172.24.1.50 and 172.24.1.150 with a 12 hour lease time  
        
    You can check with your mobile by trying to access to the RPi3's AP. (RPi3-AP & raspberry)
    
### Install MQTT broker on the RPi.

    sudo easy_install --upgrade pip
    sudo pip install paho-mqtt
    cd ~
    mkdir tmp
    cd tmp
    wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
    sudo apt-key add mosquitto-repo.gpg.key
    cd /etc/apt/sources.list.d/
    sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list
    sudo apt-get update
    sudo apt-get install mosquitto mosquitto-clients

### Install dependencies
    
    sudo pip install paho-mqtt

    
## Working with NodeMCU

### Arduino core for ESP8266 WiFi chip 

Starting with 1.6.4, Arduino allows installation of third-party platform packages using Boards Manager. 

We have packages available for Windows, Mac OS, and Linux (32 and 64 bit).

- Install **Arduino 1.6.5** from the Arduino website.
- Start Arduino and open Preferences window.
- Enter `http://arduino.esp8266.com/stable/package_esp8266com_index.json` into Additional Board Manager URLs field. 
    You can add multiple URLs, separating them with commas.
- Open Boards Manager from `Tools > Board` menu and install esp8266 platform (and don't forget to select your ESP8266 board from Tools > Board menu after installation).

### Install libraries

After you’re done installing, open the **Arduino IDE**.
 
In the menu click on `sketch -> include library -> manage libraries` and install the following libraries:
    
- **pubsubclient** by Nick O'Leary

## Compile the source code and upload to ESP.

Open the Arduino and import [sketch file](NodeMCU/MQTT_client/MQTT_client.ino).

Make sure that you have select the correct COM port of ESP.

Upload new sketch by pressing **CTRL+U**.


